# It's Nice That Webpage parser

import requests
from bs4 import BeautifulSoup
import urllib
import json
import re
import time

class ItsNiceThat:

    def __init__(self):
        self.url="https://www.itsnicethat.com/"

    def headlines_as_dict(self):
        html_doc = requests.get(self.url).text
        soup = BeautifulSoup(html_doc, 'html.parser')

        data_json = soup.find_all("script", id="props")
        data_json_parsed = json.loads(data_json[0].text)
        modules = data_json_parsed["data"]["page"]["modules"]
        content_module = [x for x in modules if "pages" in x.keys()]
        for page in content_module[0]["pages"]:
            relative_link = page["url"]
            abs_link = urllib.parse.urljoin(self.url, relative_link)
            img_path = page["listingImage"]
            if img_path:
                img_path = img_path["src"] 

            yield {
                    "title": page["title"],
                    "link": abs_link,
                    "img": img_path
                    }

class VeryGoods:

    def __init__(self):
        self.url="https://verygoods.co/"

    def headlines_as_dict(self):
        html_doc = requests.get(self.url).text
        soup = BeautifulSoup(html_doc, 'html.parser')

        data_json = soup.find_all("script", id="products")
        products = json.loads(data_json[0].text)
        for product in products:
            relative_link = product["_links"]["self"]["href"] + "/" + product["slug"]
            relative_link = relative_link.replace("products", "product")
            abs_link = urllib.parse.urljoin(self.url, relative_link)
            img_path = product["medium_image_url"]

            yield {
                    "title": product["title"],
                    "link": abs_link,
                    "img": img_path
                    }

class Pitchfork:

    def __init__(self):
        self.url="https://pitchfork.com/reviews/albums/"

    def headlines_as_dict(self):
        html_doc = requests.get(self.url).text
        soup = BeautifulSoup(html_doc, 'html.parser')

        for rdiv in soup.find_all("div", class_="review"):
            relative_link = rdiv.find("a", class_="review__link").get("href")
            abs_link = urllib.parse.urljoin(self.url, relative_link)
            img = rdiv.find("div", class_="review__artwork").find("img").get("src")
            artist = rdiv.find("ul", class_="review__title-artist").get_text()
            album = rdiv.find("h2", class_="review__title-album").get_text()
            genre = rdiv.find("ul", class_="genre-list")
            # Sometimes there's no genre.
            if genre:
                genre = genre.get_text()
            else:
                genre = "no genre"

            headline = f"{artist} - {album} ({genre})"

            yield {
                    "title": headline,
                    "link": abs_link,
                    "img": img
                    }

class PresentAndCorrect:

    def __init__(self):
        self.url="http://blog.presentandcorrect.com/"

    def headlines_as_dict(self):
        html_doc = requests.get(self.url).text
        soup = BeautifulSoup(html_doc, 'html.parser')

        for rdiv in soup.find_all("div", class_="post-main"):
            abs_link = rdiv.find("h1").find("a").get("href")
            title = rdiv.find("h1").find("a").get_text()
            imgdiv = rdiv.find("img")
            if imgdiv != None:
                img = imgdiv.get("src")
                srcset = rdiv.find("img").get("srcset")
                date_match = re.search('uploads/(\d{4})/(\d{2})/', srcset)
                year = date_match[1]
                month = date_match[2]
                date = f"{year}{month}01"
            else:
                img = None
                date = None

            yield {
                    "title": title,
                    "link": abs_link,
                    "img": img,
                    "date": date
                    }

class HackerNews:

    def __init__(self):
        self.url="https://news.ycombinator.com/"

    def headlines_as_dict(self):
        html_doc = requests.get(self.url).text
        soup = BeautifulSoup(html_doc, 'html.parser')

        for rdiv in soup.find_all("tr", class_="athing"):
            # abs_link = rdiv.find("a", class_="titlelink").get("href")
            title = rdiv.find("a", class_="titlelink").get_text()
            for link in rdiv.next_sibling.find("td", class_="subtext").find_all("a"):
                print(link)
                if "comments" in link.get_text():
                    relative_link = link.get("href")

            abs_link = urllib.parse.urljoin(self.url, relative_link)

            print(abs_link)
            yield {
                    "title": title,
                    "link": abs_link,
                    "img": None
                    }

class RssFeed:

    def __init__(self, url):
        self.url= url

    def headlines_as_dict(self):
        html_doc = requests.get(self.url).text
        soup = BeautifulSoup(html_doc, 'xml')

        for rdiv in soup.find_all("item"):
            abs_link = rdiv.find("link").get_text()
            title = rdiv.find("title").get_text()

            yield {
                    "title": title,
                    "link": abs_link,
                    "img": None
                    }

class Reddit:

    def __init__(self, subreddit):
        self.url= f"https://old.reddit.com/r/{subreddit}/"

    def headlines_as_dict(self):
        headers = {
                'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:102.0) Gecko/20100101 Firefox/102.0',
                }
        time.sleep(3) # don't trigger the bot police!
        html_doc = requests.get(self.url, headers=headers).text
        soup = BeautifulSoup(html_doc, 'html.parser')

        for thingdiv in soup.find_all("div", class_="thing"):
            if "stickied" in thingdiv['class'] or "promoted" in thingdiv['class']:
                continue

            title = thingdiv.find("a", class_="title").get_text()
            abs_link = thingdiv.find("a", class_="comments").get("href")
            date = thingdiv.find("time").get("datetime")[:10]
            img = thingdiv.find("img")

            if img:
                img = img.get("src").replace('//', '')
                img = f"'http://{img}'"

            yield {
                    "title": title,
                    "link": abs_link,
                    "img": img,
                    "date": date
                    }
