from setuptools import setup

setup(
    name='myhomepage',
    version='0.1.1',
    scripts = ['./generatehomepage.py', 'parsers.py'],
   # install_requires=[
   #     'beautifulsoup4',
   #     'requests'
   # ],
    entry_points={
        'console_scripts': ['myhomepage = generatehomepage:main']
    }
)
