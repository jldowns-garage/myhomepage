from parsers import *
from datetime import datetime
import traceback


def headline_block(title, headline_dict_list):
    """Converts a list of headline dicts into an html div."""
    html = f"""
    <div class="headline_list">
        <h2> {title} </h2>
        <ul>
    """ 

    for d in headline_dict_list:

        # add img tag if it exists
        if d['img']:
            image_tag = f"<img src = {d['img']} class='headline_img'>"
        else:
            image_tag = ""

        html += f"""
            <li>
                <a href = "{d['link']}"> {d['title']} </a>
            </li>
            """

    html += f"""
        </ul>
    </div>
    """
    return html

def text_block(title, text):
    """Just displays text. Can be HTML."""
    html = f"""
    <div class="headline_list">
        <h2> {title} </h2>
        <div>
            {text}
        <div>
    </div>
    """
    return html


def gallery_block(title, headline_dict_list):
    """Converts a list of headline dicts into an html div."""
    html = f"""
    <div class="headline_list">
        <h2> {title} </h2>
    """ 

    for d in headline_dict_list:

        # add img tag if it exists
        if d['img']:
            image_tag = f"<img src = {d['img']} class='gallery_img'>"
        else:
            image_tag = ""

        html += f"""
            <div class=gallery_item>
                <a href='{d['link']}'>
                    {image_tag}
                </a>

            </div>
            """

    html += f"""
    </div>
    """
    return html

def main():
    with open("./index.html", "w") as f:

        # write front matter
        f.write("""
        <html>
            <head>
                <title>Homepage</title>
                <style>

                    div.generatedOn {
                        width:99%;
                        height:30px;
                        float:left;
                    }

                    div.headline_list {
                        height: 300px;
                        width: 300px;
                        float: left;
                        margin: 20px;
                    }


                    div.headline_item {
                        margin-bottom: 10px;
                        float: left;
                    }

                    ul {
                        padding: 0px;
                    }

                    li {
                        font-size: 0.8em;
                        font-family: Arial, Helvetica, sans-serif;
                        margin-bottom: 10px;
                        list-style-type:none;
                    }

                    div.gallery_item {
                        float:left;
                    }

                    img.gallery_img{
                        height: 100px;
                        margin-right: 1px;
                    }
                
                
                </style>
            </head>
        <body>
        """)

        # Hacker News
        try:
            parser = HackerNews()
            headline_dicts = list(parser.headlines_as_dict())[:5]
            html_block = headline_block("Hacker News", headline_dicts)
        except Exception as e:
            html_block = text_block("Hacker News", f"{e} {traceback.format_exc()}")
        f.write(html_block)

        # Its Nice That
        try:
            parser = ItsNiceThat()
            headline_dicts = list(parser.headlines_as_dict())[:5]
            html_block = headline_block("It's Nice That", headline_dicts)
        except Exception as e:
            html_block = text_block("It's Nice That", f"{e} {traceback.format_exc()}")
        f.write(html_block)

        # Swiss Miss
        try:
            parser = RssFeed("https://www.swiss-miss.com/feed")
            headline_dicts = list(parser.headlines_as_dict())[:5]
            html_block = headline_block("Smiss Miss", headline_dicts)
        except Exception as e:
            html_block = text_block("Smiss Miss", f"{e} {traceback.format_exc()}")
        f.write(html_block)


        # VeryGoods.co
        try:
            parser = VeryGoods()
            headline_dicts = list(parser.headlines_as_dict())[:6]
            html_block = gallery_block("Very Goods", headline_dicts)
        except Exception as e:
            html_block = text_block("Very Goods", f"{e} {traceback.format_exc()}")
        f.write(html_block)

        # Pitchfork
        try:
            parser = Pitchfork()
            headline_dicts = list(parser.headlines_as_dict())[:5]
            html_block = headline_block("Pitchfork", headline_dicts)
        except Exception as e:
            html_block = text_block("Pitchfork", f"{e} {traceback.format_exc()}")
        f.write(html_block)

        # Present and Correct
        try:
            parser = PresentAndCorrect()
            headline_dicts = list(parser.headlines_as_dict())[:5]
            html_block = headline_block("Present and Correct", headline_dicts)
        except Exception as e:
            html_block = text_block("Present and Correct", f"{e} {traceback.format_exc()}")
        f.write(html_block)

        #####
        # Reddit Zone
        #####

        try:
            parser = Reddit("functionalprint")
            headline_dicts = list(parser.headlines_as_dict())[:5]
            html_block = headline_block("Functional Print", headline_dicts)
        except Exception as e:
            html_block = text_block("Functional Print", f"{e} {traceback.format_exc()}")
        f.write(html_block)

        try:
            parser = Reddit("fitness")
            headline_dicts = list(parser.headlines_as_dict())[:5]
            html_block = headline_block("Normal Fitness", headline_dicts)
        except Exception as e:
            html_block = text_block("Normal Fitness", f"{e} {traceback.format_exc()}")
        f.write(html_block)

        try:
            parser = Reddit("bodyweightfitness")
            headline_dicts = list(parser.headlines_as_dict())[:5]
            html_block = headline_block("Bodyweight Fitness", headline_dicts)
        except Exception as e:
            html_block = text_block("Bodyweight Fitness", f"{e} {traceback.format_exc()}")
        f.write(html_block)

        try:
            parser = Reddit("Monero")
            headline_dicts = list(parser.headlines_as_dict())[:5]
            html_block = headline_block("Monero", headline_dicts)
        except Exception as e:
            html_block = text_block("Monero", f"{e} {traceback.format_exc()}")
        f.write(html_block)

        try:
            parser = Reddit("Bitcoin")
            headline_dicts = list(parser.headlines_as_dict())[:5]
            html_block = headline_block("Bitcoin", headline_dicts)
        except Exception as e:
            html_block = text_block("Bitcoin", f"{e} {traceback.format_exc()}")
        f.write(html_block)

        try:
            parser = Reddit("unixporn")
            headline_dicts = list(parser.headlines_as_dict())[:5]
            html_block = gallery_block("Unix", headline_dicts)
        except Exception as e:
            html_block = text_block("Unix", f"{e} {traceback.format_exc()}")
        f.write(html_block)

        try:
            parser = Reddit("Bujo")
            headline_dicts = list(parser.headlines_as_dict())[:5]
            html_block = headline_block("bujo", headline_dicts)
        except Exception as e:
            html_block = text_block("bujo", f"{e} {traceback.format_exc()}")
        f.write(html_block)

        # Sign and date
        f.write(f"""
                <div class="generatedOn">Generated on {datetime.now().strftime("%d/%m/%Y %H:%M:%S")}</div>

        """)

        # epilog
        f.write("</body></html>")


if __name__ == "__main__":
    main()
